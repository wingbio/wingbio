import style from './style.css';

const HorizontalLine = ({ type }) => (
  <div class={style.wraper}>
    <div class={type === 'white' ? style.circleWhite : style.circle} />
    <div class={type === 'white' ? style.lineWhite : style.line} />
    <div class={type === 'white' ? style.circleWhite : style.circle} />

  </div>

);

export default HorizontalLine;
