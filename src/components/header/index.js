import { Link } from 'preact-router/match';
import burger from '../../assets/icons/burger.png'
import style from './style.css';
import { Component as Logo } from '../../assets/Group 18.svg';
import logo from '../../assets/icons/logo.png'
import { useState, useEffect } from 'preact/hooks';

const Header = () => {

  useEffect(() => {
    if (!loaded) {
      document.getElementById(id).scrollIntoView()
      setLoaded(true)
    }
  }, loaded);

  const [isBurgerOpen, setBurgerOpen] = useState(0);
  const [loaded, setLoaded] = useState(true)
  const [id, setId] = useState('');
  const [activeNav, setActiveNav] = useState('top');

  const boundFun = () => {
    if (window.pageYOffset > 110) {
      document.getElementById("header").style.cssText = "background: rgba(0,0,0,0.7)";

    } else {
      document.getElementById("header").style.cssText = "background: transparent";
    }

    var coverHeight = document.getElementById('cover').offsetHeight - 100;
    var x = window.matchMedia("(min-width: 1200px)")

    if (window.pageYOffset > coverHeight && x.matches) {
      document.getElementById("header").style.cssText = `
      background: rgba(0,0,0); 
      background-position: right; 
      background-repeat: no-repeat;
      height: 70px;
      `;
      document.getElementById("logo").style.cssText = `width: 60px`;
    }
  }

  window.addEventListener("scroll", boundFun, false);

  const navigate = id => {
    setActiveNav(id);
    if (id === 'top') {
      window.scrollTo(0, 0);
    } else {
      let rout = window.location.pathname.split('/')
      if (rout[1] !== 'partners') {
        document.getElementById(id).scrollIntoView({ behavior: "smooth" })
      }
    }
    setBurgerOpen(false);
    setId(id)
    setLoaded(false)

  }

  return (
    <header class={style.header} id="header">
      {/* <Logo style={{width: 100, height: 100}}/> */}
      <img src={logo} class={style.logo} id="logo" />
      <nav>
        <Link class={activeNav === 'top' ? style.active : null} href="/#" onClick={() => navigate('top')}>HOME</Link>
        <Link class={activeNav === 'approach' ? style.active : null} href="/#approach" onClick={() => navigate('approach')}>APPROACH</Link>
        <Link class={activeNav === 'team' ? style.active : null} href="/#team" onClick={() => navigate('team')}>TEAM</Link>
        <Link activeClassName={style.active} href="/partners">PARTNERSHIPS</Link>
        <Link class={activeNav === 'contact' ? style.active : null} href="/#contact" onClick={() => navigate('contact')}>CONTACT</Link>
      </nav>
      <img src={burger} class={style.burger} onClick={() => setBurgerOpen(!isBurgerOpen)} />
      <div class={isBurgerOpen ? style.burgerMenu : style.burgerHide}>
        <Link class={activeNav === 'top' ? style.active : null} href="/#" onClick={() => navigate('top')}>HOME</Link>
        <Link class={activeNav === 'approach' ? style.active : null} href="/#approach" onClick={() => navigate('approach')}>APPROACH</Link>
        <Link class={activeNav === 'team' ? style.active : null} href="/#team" onClick={() => navigate('team')}>TEAM</Link>
        <Link activeClassName={style.active} href="/partners" onClick={() => setBurgerOpen(false)}>PARTNERSHIPS</Link>
        <Link class={activeNav === 'contact' ? style.active : null} href="/#contact" onClick={() => navigate('contact')}>CONTACT</Link>
      </div>
    </header>
  )

};

export default Header;
