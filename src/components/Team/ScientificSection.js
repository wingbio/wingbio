import style from './style.css';
import { peopleBottom } from '../../assets/people';
import CircleSection from './CircleSection';
import HorizontalLine from '../HorizontalLine/HorizontalLine';

const ScientificSection = () => (
  <div class={style.scientific}>
    <div class={style.scientificMask} />
    <div class={style.scientificContent}>
      <h1 class={style.teamText}>Scientific Advisory Board</h1>
      <HorizontalLine type="white" />
      <div class={style.teamFlex2}>
        <div class={style.helper}>
          {peopleBottom.map(item => (
            <CircleSection img={item.img} name={item.name} position={item.position} text={item.text} name2={item.name2}/>
          ))}
        </div>
      </div>
    </div>
  </div>
);

export default ScientificSection;
