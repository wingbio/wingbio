import style from './style.css';
import CircleSection from './CircleSection';
import { people } from '../../assets/people';
import ScientificSection from './ScientificSection';
import HorizontalLine from '../HorizontalLine/HorizontalLine';


const TeamSection = () => (
  <div class={style.teamSection} id="team">
    <h1 class={style.teamText}>TEAM</h1>
    <HorizontalLine type="white" />
    <div class={style.teamFlex}>
      <div class={style.helper}>
        {people.map(item => (
          <div>
            <CircleSection img={item.img} name={item.name} position={item.position} text={item.text} name2={item.name2} />
          </div>
        ))}

      </div>


    </div>
    <ScientificSection />
  </div>

);

export default TeamSection;
