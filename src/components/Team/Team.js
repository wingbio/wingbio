import style from './style.css';
import TeamSection from './TeamSection';

const Team = () => (
  <div class={style.team}>
    <TeamSection />
  </div>
);

export default Team;
