import style from './style.css';
import { useState } from 'preact/hooks';

const CircleSection = ({ img, name, position, text, name2 }) => {

  return (
    <div class={style.wraper}>
      <div class={style.test} >
        <div class={style.circleMask} >
          <img intrinsicsize="200x200" src={img} class={style.circle} />
        </div>
        <h1 class={style.name}>{name}</h1>
        <span class={style.position}>{position}</span>
        <div class={style.tooltip} id="tooltip">
          <p>
            <b>{name2}</b> {text}
          </p>
        </div>
      </div>
    </div>
  )
}


export default CircleSection;
