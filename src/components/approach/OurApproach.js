import style from './style.css';
import img1 from '../../assets/Group 634.png';
import img2 from '../../assets/Group 635.png';
import HorizontalLine from '../HorizontalLine/HorizontalLine';
// import img1 from '../../assets/Group 634.png'

const OurApproach = () => (
  <div class={style.section} id="approach">
    <div class={style.sectionHeader} />
    <div>
      <h1 className={style.sectionTitle}>Our Approach</h1>
      <HorizontalLine />
    </div>
    <div>
      <p class={style.text}>
        <b>WingBio</b> uses state-of the-art deep learning tools, customized to your data, to analyze large microscopy image datasets and complex biological phenomena. 
        <br /><br />
        Our high-performance algorithms solve challenging problems in microscopy image analysis such as 3D cell segmentation, detection of subtle cell features, and phenotypic classification in High-content screening. 
        <br /><br />
        Benefits: 
        <br />
        • Reduces the need for the use multiple markers and fluorescent channels, thereby reducing costs 
        <br />
        • Reduces the need for time-consuming image analysis and manual cell image segmentation, speeding up drug discovery 
        <br />
        • Improves accuracy by reducing subjectivity and variability and enables detection of subtle features, producing higher quality data in comparison to manual analysis
        <br /><br />
         We use cutting-edge deep learning algorithms to extract subtle cellular features from large Z-stack image data sets to develop predictive cell segmentation models. By automatizing the processes of 3D segmentation, quantification of morphology and texture, and phenotypic classification of cells in 3D cultures treated with different therapeutics, we are contributing to more reliable, cost-effective and time-saving drug discovery.
      </p>
    </div>
    <div class={style.blueTextContainer}>
      <p class={style.blueText}>
      Our deep learning algorithms enable analysis of multiple parameter readouts (e.g. spheroid cluster <br />diameter, fluorescence intensity, spheroid/cluster volume, cell counts, distance between cells),   reducing <br />the need for multiple fluorophore staining with advanced analysis of bright-field images
      </p>
    </div>
    <div class={style.imgSection}>
      <img src={img1} class={style.img1}/>
      <img src={img2} class={style.img2}/>
    </div>
  </div>
);

export default OurApproach;
