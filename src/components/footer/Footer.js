import style from './style.css';
import { useState } from 'preact/hooks';

var Email = { send: function (a) { return new Promise(function (n, e) { a.nocache = Math.floor(1e6 * Math.random() + 1), a.Action = "Send"; var t = JSON.stringify(a); Email.ajaxPost("https://smtpjs.com/v3/smtpjs.aspx?", t, function (e) { n(e) }) }) }, ajaxPost: function (e, n, t) { var a = Email.createCORSRequest("POST", e); a.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), a.onload = function () { var e = a.responseText; null != t && t(e) }, a.send(n) }, ajax: function (e, n) { var t = Email.createCORSRequest("GET", e); t.onload = function () { var e = t.responseText; null != n && n(e) }, t.send() }, createCORSRequest: function (e, n) { var t = new XMLHttpRequest; return "withCredentials" in t ? t.open(e, n, !0) : "undefined" != typeof XDomainRequest ? (t = new XDomainRequest).open(e, n) : t = null, t } };

const Footer = () => {

  const [email, setEmail] = useState('');
  const [text, setText] = useState('');
  const [title, setTitle] = useState('');
  const [errorText, setErrorText] = useState('All fields are required')

  const validateEmail = (mail) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(mail).toLowerCase());
  }

  const sendEmail = () => {

    setErrorText('All fields are required');

    document.getElementById('nameError').style.cssText = `display: none;`
    document.getElementById('nameInput').style.cssText = `border: 1px solid rgba(256,256,256,0.5);`
    document.getElementById('e-mailInput').style.cssText = `border: 1px solid rgba(256,256,256,0.5);`
    document.getElementById('text-input').style.cssText = `border: 1px solid rgba(256,256,256,0.5);`
  
    if (title === "") {
      document.getElementById('nameError').style.cssText = `display: block;`
      document.getElementById('nameInput').style.cssText = `border: 1px solid red;`
    }

    if (email === "") {
      document.getElementById('nameError').style.cssText = `display: block;`
      document.getElementById('e-mailInput').style.cssText = `border: 1px solid red;`
    }

    if (text === "") {
      document.getElementById('nameError').style.cssText = `display: block;`
      document.getElementById('text-input').style.cssText = `border: 1px solid red;`
    }

    if(title !== '' && text !== '' && email !== '' && validateEmail(email) === false){
      setErrorText('E-mail is not in valid format')
      document.getElementById('nameError').style.cssText = `display: block;`
      document.getElementById('e-mailInput').style.cssText = `border: 1px solid red;`
    } 

    if (validateEmail(email) && title !== '' && text !== '' ) {
      Email.send({
        Host: "smtp.gmail.com",
        enableSsl: "true",
        Username: "wingmansender@gmail.com",
        Password: 'wingmansendermail',
        To: "zm@wingman.ai, zz@wingman.ai, ig@wingman.ai, jo@wingman.ai",
        From: email,
        Subject: title,
        Body: text,
      }).then(
        message => alert('Message sent successfully!')
      );
    }
  }


  return (

    <div class={style.footer} id="contact">
      <div class={style.footerContent}>
        <div class={style.footerFlex}>
          <p class={style.footerHeader}>Contact Us</p>
          <h1 class={style.footerSubHeader}>Get in touch!</h1>
          <p class={style.footerText}>Feel free to fill in the form for any questions or suggestions you may have! We will get back to you as soon as possible!</p>
          {/* <form method="post"> */}
          <div class={style.form}>
            <span id="nameError" class={style.nameError} >{errorText}</span>
            <div class={style.formInputTop}>
              <input id="nameInput" type="text" placeholder="Name" class={[style.input, style.input1].join(' ')} onChange={e => setTitle(e.target.value)} />
              <input id="e-mailInput" type="email" placeholder="Email" class={style.input} onChange={e => setEmail(e.target.value)} />
            </div>
            <textarea id="text-input" class={style.textarea} placeholder="Message" onChange={e => setText(e.target.value)} />
            <input type="submit" value="SEND" class={style.formBtn} onClick={() => sendEmail()} />
          </div>
          {/* </form> */}
        </div>

      </div>

    </div>
  )
}

export default Footer;
