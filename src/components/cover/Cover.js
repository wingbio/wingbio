import style from './style.css';

const Cover = () => (
  <div class={style.cover} id="cover">
    <p class={style.headerText}>
      Artificial Intelligence Microscopy Image Analysis Drug Discovery
    </p>
  </div>
);

export default Cover;
