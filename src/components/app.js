import { Component } from 'preact';
import { Router } from 'preact-router';

import Header from './header';

import Home from '../routes/home';
import Partners from '../routes/partners';

export default class App extends Component {

  handleRoute = e => { this.currentUrl = e.url; };

  render() {
    return (
      <div id="app">
        <div id="loader" />
        <Header />
        <Router onChange={this.handleRoute}>
          <Home path="/" />
          <Home path="/contact" />
          <Partners path="/partners/"/>
        </Router>
      </div>
    );
  }
}
