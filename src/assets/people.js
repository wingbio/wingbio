const people = [
  {
    text: `has launched and revived consumer-facing internet businesses, and led them to dramatic growth andsuccess. 
    Since 2013 he has been focused on AI insights and tools for 
    internet-facing consumer businesses, and launched VAIX.ai with his co-founders in 2016 to bring 
    those concepts to market. The work at VAIX led to the vision and roadmap for Wingman.ai. 
    He realised that every company has the 
    same problem: non-AIexperts in every company need an intuitive way to interact with and 
    extract insights from data using AI, and human-directed AI agents were the result.`,
    name: 'John O’Malia',
    name2: 'John',
    position: 'Founder / CEO',
    img: require('./people/John.png')
  },
  {
    text: `upon completing PhD in Cognitive Science at the University of Warwick, UK, focusing on understanding the development and mechanisms of human selective attention in different visual contexts. Prior to this
     she worked as a psychologist in healthcare services. Upon completing her PhD, she workedas a Research Associate in Public Health and Primary Care at the University of Cambridge before transitioning to the pharmaceutical 
     industry, where she worked as a Clinical Outcomes Assessment Scientist at Parexel in London, UK. She joined Wingbio as Head of Partnerships working across both product and business.`,
    name: 'Zorana Zupan',
    name2: 'Zorana',
    position: 'Head of Partnerships',
    img: require('./people/Zorana.png')
  },
  {
    text: `completed her PhD in Molecular Biiology, Cancer Research at the University of Belgrade. During her PhD she worked at 
    the Faculty of Medicine in Ciudad Real, 
    Spain, where she studied the effects of different drugs on cancer cell migration and invasion. She has also worked at the 
    Faculty of Medicine in Larissa,Greece where she studied in vivo 
    cancer models (SCID, NSG mice). She is authored 14 scientific research papers published in high impact 
    journals. She has participated in more than 20 international conferences 
    and workshops. Her PhD was awarded as the best PhD in Molecular Biology in 2014 in Serbia. She started her clinical research 
    career in 2016 as a Medical Affairs Scientist at SyneosHealth (Previously Inc Research - Inventiv Health). She joined Wingbio as Scientific consultant and Product Manager.`,
    name: 'Zorica Milosevic',
    name2: 'Zorica',
    position: 'Scientist, Product Manager',
    img: require('./people/Zorica.png')
  },
  {
    text: `started with procedural programming in 1991 using a Sinclair ZX Spectrum BASIC at the age of just 10. He did his MSc in AI on the subject of Fully Connected Neural Networks, 
    and started his professional career at Monster.com. Ivan later progressed to Head of Engineering in a company developing security and facial recognition software for three years, 
    having started as Tech Lead. He returned to AI in 2016 as a Java One speaker on Deep Learning, and ran projects including seabed object detection software for sonarmaps in submarines. 
    Ivan has a broad, bottom-up understanding of software development, software architecture, development pipeline and agile methods.`,
    name: 'Ivan Goloskokovic',
    name2: 'Ivan',
    position: 'Chief Technology Officer',
    img: require('./people/Ivan.png')
  },
  {
    text: `is a machine learning engineer at Wingbio, presently focused on evolving human-assistive reinforcement learning systems. He has previously worked a scalable computer vision 
    system for seabed image object detection for sonar maps on submarines. Most of his time is spent reading pioneering 
    machine learning papers, with a focus on converting these ideas to 
    into something useable. Dusan also has experience with satellite image segmentation, object classification and localisation, generative-adversarial networks, autoencoders and genetic algorithms.`,
    name: 'Dusan Josipovic',
    name2: 'Dusan',
    position: 'Co-Lead Developer',
    img: require('./people/Dusan.png')
  },
  {
    text: `is a machine learning engineer with experience in deep 
    learning, computer vision, and data science. Currently, he works on 
    combining reinforcement learning with human directives at Wingbio, 
    creating new models, optimising the training pipeline and reading and 
    implementing state-of-the-art papers. Previously, he worked at a UK 
    company where he was one of the main engineers on a 
    large-scale computer vision analysis project, where the goal was to 
    segment sonar images on petabytes of data using deep learning. He has 
    also worked on numerous other deep learning projects, developing these solutions from 
    idea to production using cutting-edge algorithms. His expertise includes 
    satellite image segmentation, large-scale sentiment analysis, casino table 
    object detection, comic colourisation and even face swap.`,
    name: 'Nikola Jovicic',
    name2: 'Nikola',
    position: 'Co-Lead Developer',
    img: require('./people/Nikola.png')
  },
]

const peopleBottom = [
  {
    text: `obtained her PhD in molecular biology at The Faculty of Biology, 
     University of Belgrade. She is a Senior Research Associate at the Institute for Biological 
     Research „Siniša Stanković“ - National Institut of the Republic of Serbia, Univeristy of Belgrade. Her area of expertise is in cancer 
     reasearch and she has 15 years of experience in preclinical evaluation of novel anti-cancer 
     agents. Recently, she has focused her research on 
     application of 3D cell culture models in drug discovery and development. At the begining of her 
     career she completed an eight month intership at the University of Iowa,USA, Department of 
     Physiology and Biophysics. She has participated in
      several national and international projects and collaborative consortium. She was awarded for 
      “The Best Master’s Thesis in Molecular Biology” in 2008 and mentored a PhD theisis that was awarded 
      as “The Best PhD Thesis in Molecular Biology” in 
      2018. She was a member of the team that received second place award in the field of inovations in 
      biology at the competition for “The Best Technological Innovation in Serbia” in 2008. Dr.Stanković is 
      author and co-author of 34 scientific 
      research papers in highly ranked journals, 3 book chapters and more than 80 conference reports.`,
    name: 'Tijana Stankovic',
    name2: 'Tijana',
    position: 'Senior Scientist',
    img: require('./people/tijana.png')
  },
  {
    text: `is a highly skilled and experienced with years of experience in bio informatics and cancer research for translational studies. He holds a Ph.D. in Molecular Biology. He has significant expertise in 
    bioinformatics and statistics, and relevant software packages for data analysis, fluorescent microscopy, and many areas of research that utilize in vivo, ex vivo and in vitro skills.`,
    name: 'Miodrag Dragoj',
    name2: 'Miodrag',
    position: 'Bioinformatician',
    img: require('./people/Miodrag.png')
  },
]

export {
  people,
  peopleBottom
}