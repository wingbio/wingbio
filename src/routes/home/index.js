import { h } from 'preact';
import style from './style';
import Cover from '../../components/cover/Cover';
import OurApproach from '../../components/approach/OurApproach';
import Team from '../../components/Team/Team';
import Footer from '../../components/footer/Footer';
import { useEffect} from 'preact/hooks';

document.getElementById('loader').style.cssText = 'display: none'
 

const Home = () => {
  useEffect(() => {
    let rout = window.location.pathname.split('/')
    if(rout[1] === 'contact'){
      window.scrollTo(0,document.body.scrollHeight);
    }
  }, []);
  
  return (
    <div class={style.home}>
      <Cover />
      <OurApproach />
      <Team />
      <Footer />
    </div>
  )
}


export default Home;
