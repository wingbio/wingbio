import style from './style';
import headerImg from '../../assets/backgrounds/Image 1.png'
import { route } from 'preact-router';

const Partners = () => {
  return (
    <div class={style.profile}>
      <div class={style.cover} id="cover" />
      <div class={style.container}>
        <div class={style.content}>
          <div class={style.header}>
            <img src={headerImg} class={style.img} />
            <p class={style.headerText}><i>University of Belgrade <br /> Institute for Biological Research “Siniša Stanković” <br />National Institute of Republic of Serbia</i></p>
          </div>
          <div>
            <h1 class={style.mainHeader}>Laboratory for Molecular Neuro-Oncology</h1>
            <p class={style.boldText}>
              Project: Investigation of cellular and molecular mechanisms of glioblastoma pathogenesis, its invasion and response to therapy <br />
            </p>
            <p class={style.regularText}>
              The Laboratory for Molecular Neuro-Oncology’s primary research interests are focused on investigation of cellular and molecular mechanisms of glioblastoma pathogenesis, its invasion and response to therapy as well as other types of cancers, lung, breast, thyroid and colorectal carcinomas. Areas of expertise include preclinical in vitro and in vivo testing of novel anticancer compounds, on both a pharmacodynamic and mechanistic level. Areas of interest include studying cellular and molecular mechanisms of resistance to anticancer agents and potential for overcoming such treatment failure. The Laboratory’s research is focused on developing in vitro models for studying drug resistance and screening compounds with drug resistance reversal potential. Current projects include the development of innovative 3D cell culture platform for improved drug screening, therapy optimization and selection of personalized treatments. Fluorescent and confocal microscopy are routinely used in the lab, generating a large amount of data that we use for development and validation of our AI platforms.
          </p>
            <div class={style.borderedText}>
              <p class={style.bText}>We are providing our state-of-the-art artificial intelligence technology to our partners, customized to your data and area of application. Please <span class={style.link}><b  onClick={() => route('/contact')}>contact us</b></span> if you would like to learn more about our technology and for potential collaborations</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}


export default Partners;